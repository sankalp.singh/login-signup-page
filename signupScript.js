const email = document.getElementById("email");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirm-password");
const age = document.getElementById("age");
const form = document.getElementById("signup");
const errorElement = document.getElementById("error");
const emailError = document.getElementById("email-error");
const passwordError = document.getElementById("password-error");
const ageError = document.getElementById("age-error");
const repeatPasswordError = document.getElementById("repeat-password-error");

const emailRegex =
	/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;

form.addEventListener("submit", (e) => {
	let message = [];
	if (
		email.value === "" ||
		email.value == null ||
		!emailRegex.test(email.value)
	) {
		message.push("Invalid email");
		emailError.innerText = "* Invalid email";
		email.focus();
		e.preventDefault();
	}

	if (!strongRegex.test(password.value)) {
		message.push("Invalid password");
		passwordError.innerText = "* Invalid Password";
		password.focus();
		e.preventDefault();
	}
	if (password.value !== confirmPassword.value) {
		message.push("Your password doesn't match!");
		repeatPasswordError.innerText = "* Your password doesn't match!";
		confirmPassword.focus();
		e.preventDefault();
	}
	if (age.value === "" || age.value === null) {
		message.push("Age cannot be empty");
		ageError.innerText = "* Age cannot be empty";
		age.focus();
		e.preventDefault();
	} else if (age.value < 18) {
		message.push("You are under age!");
		ageError.innerText = "* You are under age!";
		e.preventDefault();
	}
	if (message.length > 0) {
		e.preventDefault();
		// errorElement.innerHTML = message.join("<br>");
	}
});
