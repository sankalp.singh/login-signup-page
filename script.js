const email = document.getElementById("email");
const password = document.getElementById("password");
const form = document.getElementById("login");
const errorElement = document.getElementById("error");
const emailError = document.getElementById("email-error");
const passwordError = document.getElementById("password-error");

const emailRegex =
	/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;

form.addEventListener("submit", (e) => {
	let message = [];
	if (
		email.value === "" ||
		email.value == null ||
		!emailRegex.test(email.value)
	) {
		message.push("Invalid E-mail");
		emailError.innerText = "*Invalid Email!";
		email.focus();
		e.preventDefault();
	}
	if (!strongRegex.test(password.value)) {
		e.preventDefault();
		message.push("Invalid password");
		passwordError.innerText = "*Invalid Password";
		password.focus();
	}
	if (message.length > 0) {
		e.preventDefault();
		// errorElement.innerHTML = message.join("<br>");
	}
});
